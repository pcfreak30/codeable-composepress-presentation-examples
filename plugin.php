<?php

namespace MyPlugin\Plugin;

// MyPlugin\Core\Plugin gets generated
use MyPlugin\Core\Plugin as PluginBase;

class Plugin extends PluginBase {
	const VERSION = '0.1.0';
	const PLUGIN_SLUG = 'my-plugin';
	const PLUGIN_NAMESPACE = '\MyPlugin';


	protected function setup(){
		// This is always optional
	}

	public function activate( $network_wide ) {
	}

	public function deactivate( $network_wide ) {
	}

	protected function load_components() {
	}

	protected function get_dependencies_exist() {
		return true;
	}
}
