<?php

namespace MyPlugin;

use MyPlugin\Core\Component;

class Admin extends Component {
	// This is always optional, but is where all business logic goes
	public function setup() {
		add_action( 'admin_menu', [ $this, 'register_menu' ] );
	}

	public function register_menu() {
		add_menu_page(
			__( 'Custom Menu Title', $this->plugin->safe_slug ),
			'custom_menu',
			'manage_options',
			'my_plugin_settings',
			[ $this, 'render_settings' ]
		);
	}

	public function render_settings() {
		?>
		<h1>Howdy!</h1>
		<?php
	}
}
